var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.css.autoprefix = {
    enabled: true
};

elixir(function (mix) {
    // mix.sass('app.scss')
    mix.less(['main.less', 'properties.less'], './public/css/main.css')
        .scripts(['properties/worktable/toolbars.js', '/properties/worktable/worktable.js'], './public/js/worktable.js')

});
