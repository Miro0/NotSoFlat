$('.toolbar-wrapper').find('a').click(function () {
    $('.toolbar-wrapper').addClass('show');
});

$('.worktable').click(function () {
    // $('.toolbar-wrapper').removeClass('show');
    // setTimeout(function () {
    //     $('.toolbar-wrapper .active').removeClass('active');
    // }, 500);
});
(function ($) {
    var WorkTable = {
        canvas: document.getElementById('worktable'),
        engine: null,
        camera: null,
        scene: null,
        ratio: null,
        ground: null,
        grid: null,
        insertMode: false,
        insertModeContext: null,
        lsatSelection: null,
        cursor: null,
        cursorHelpers: {},
        materials: {},
        meshes: {},
        snapToGrid: true,
        gridPrecision: '3',
        gridPrecisionMap: {
            '1': {'label': '1', 'precision': 20, 'gridFrequency': 100, 'gridRatio': 0.05, 'align': 0.025},
            '2': {'label': '2', 'precision': 10, 'gridFrequency': 50, 'gridRatio': 0.1, 'align': 0.05},
            '3': {'label': '5', 'precision': 4, 'gridFrequency': 20, 'gridRatio': 0.25, 'align': 0.125},
            '4': {'label': '10', 'precision': 2, 'gridFrequency': 10, 'gridRatio': 0.5, 'align': 0.25},
            '5': {'label': '20', 'precision': 1, 'gridFrequency': 5, 'gridRatio': 1, 'align': 0.5},
            '6': {'label': '25', 'precision': 0.8, 'gridFrequency': 4, 'gridRatio': 1.25, 'align': 0.625},
            '7': {'label': '33,33', 'precision': 0.6, 'gridFrequency': 3, 'gridRatio': 1.6666667, 'align': 0.83333335},
            '8': {'label': '50', 'precision': 0.4, 'gridFrequency': 2, 'gridRatio': 2.5, 'align': 1.25},
            '9': {'label': '100', 'precision': 0.2, 'gridFrequency': 1, 'gridRatio': 5, 'align': 2.5},
        },
        gridAlign: 0.125,
        cameraRadiusDiff: 10,
        lastCameraRadius: null,
        panMouseButton: 0,

        init: function () {
            this.engine = new BABYLON.Engine(this.canvas, true);
            this.scene = new BABYLON.Scene(this.engine);

            this.scene.clearColor = BABYLON.Color3.White();
            this.scene.fogColor = BABYLON.Color3.White();
            this.scene.fogMode = BABYLON.Scene.FOGMODE_LINEAR;
            this.scene.fogStart = 100.0;
            this.scene.fogEnd = 150.0;
            this.scene.fogDensity = .5;

            this.camera = new BABYLON.ArcRotateCamera("camera", 0, 0, 25, new BABYLON.Vector3(0, 0, 0), this.scene);
            this.camera.setTarget(new BABYLON.Vector3(0, 0, 0));
            this.camera.checkCollisions = true;
            this.camera.lowerBetaLimit = 0;
            this.camera.upperBetaLimit = Math.PI - 1.6;
            this.camera.lowerRadiusLimit = 2;
            this.camera.upperRadiusLimit = 50;
            this.camera.mode = BABYLON.Camera.ORTHOGRAPHIC_CAMERA;
            this.camera.panningSensibility = 100;
            this.camera.attachControl(this.canvas, false, false, this.panMouseButton);

            this.grid = new BABYLON.GridMaterial('groundMaterial', this.scene);
            this.grid.minorUnitVisibility = .5;
            this.grid.gridRatio = 0.25;
            this.grid.majorUnitFrequency = 20;
            this.grid.lineColor = new BABYLON.Color3(0.8, 0.8, 0.8);

            this.ground = BABYLON.Mesh.CreateGround('ground', 500, 500, 1, this.scene);
            this.ground.material = this.grid;
            this.ground.checkCollisions = true;
            this.ground.actionManager = new BABYLON.ActionManager(this.scene);

            var light = new BABYLON.DirectionalLight('light', new BABYLON.Vector3(5, -5, 3), this.scene);
            light.target = new BABYLON.Vector3(0, 0, 0);
            light.specular = new BABYLON.Color3(0, 0, 0);

            this.materials.red = new BABYLON.StandardMaterial('red', WorkTable.scene);
            this.materials.red.specular = new BABYLON.Color3(0, 0, 0);
            this.materials.red.specularColor = new BABYLON.Color3(0, 0, 0);

            $('#zoom').val(this.camera.radius);

            this.onResize();
            this.onZoom();
        },
        onUpdate: function () {
            if (this.camera.beta === 0) {
                this.camera.panningAxis = new BABYLON.Vector3(1, 1, 0);
            } else {
                this.camera.panningAxis = new BABYLON.Vector3(1, 0, 1);
            }

            if (this.camera.radius !== this.lastCameraRadius) {
                this.onZoom();
            }
        },
        onZoom: function () {
            this.lastCameraRadius = this.camera.radius;
            this.camera.orthoTop = this.camera.radius * 0.3;
            this.camera.orthoBottom = -this.camera.radius * 0.3;
            this.camera.orthoRight = this.camera.radius * this.ratio * 0.3;
            this.camera.orthoLeft = -this.camera.radius * this.ratio * 0.3;
            $('#zoom').val(this.camera.radius);
        },
        onResize: function () {
            this.ratio = $(this.canvas).width() / $(this.canvas).height();
            $('.workarea').css('height', $(window).height() - 139);
            this.engine.resize();
        },
        run: function () {
            var that = this;
            this.engine.runRenderLoop(function () {
                that.onUpdate();
                that.scene.render();
            });
        },
        setPerspective: function (perspective) {
            this.camera.mode = perspective;
        },
        getGroundPosition: function () {
            var pick = WorkTable.scene.pick(WorkTable.scene.pointerX, WorkTable.scene.pointerY, function (mesh) {
                return mesh == WorkTable.ground;
            });
            if (pick.hit) {
                if (this.snapToGrid) {
                    var alignParamX = this.gridPrecisionMap[this.gridPrecision].align;
                    var alignParamZ = alignParamX;
                    if (pick.pickedPoint.x < 0) {
                        alignParamX *= -1;
                    }
                    if (pick.pickedPoint.z < 0) {
                        alignParamZ *= -1;
                    }
                    var x = parseInt((pick.pickedPoint.x + alignParamX) * this.gridPrecisionMap[this.gridPrecision].precision) / this.gridPrecisionMap[this.gridPrecision].precision;
                    var z = parseInt((pick.pickedPoint.z + alignParamZ) * this.gridPrecisionMap[this.gridPrecision].precision) / this.gridPrecisionMap[this.gridPrecision].precision;
                    return new BABYLON.Vector3(x, 0.01, z);
                } else {
                    return new BABYLON.Vector3(pick.pickedPoint.x, 0.01, pick.pickedPoint.z);
                }
            }
            return null;
        },
        registerClickEvent: function (clickFunction) {
            var downEvent = null;
            var upHandler = function(upEvent) {
                if (Math.abs(upEvent.x - downEvent.x) < 2 || Math.abs(upEvent.y - downEvent.y) < 2) {
                    clickFunction(upEvent);
                }
                WorkTable.canvas.removeEventListener('pointerdown', upHandler);
            };
            this.canvas.addEventListener('pointerdown', function(event) {
                downEvent = event;
                WorkTable.canvas.addEventListener('pointerup', upHandler);
            }, false);
        },
        unregisterClickEvent: function(clickFunction) {
            this.canvas.removeEventListener('pointerdown', clickFunction);
        },
        toggleMode: function () {
            switch (this.insertModeContext) {
                case 'walls':
                    var pointerMoveAction = function (event) {
                        var groundPosition = WorkTable.getGroundPosition();
                        if (groundPosition !== null) {
                            WorkTable.cursor.position.x = groundPosition.x;
                            WorkTable.cursor.position.z = groundPosition.z;
                            if (typeof(WorkTable.cursorHelpers['wallLine']) !== 'undefined' && WorkTable.lastSelection !== null) {
                                // WorkTable.cursorHelpers['wallLine']['lines'] = [new BABYLON.Vector3(WorkTable.cursor.position.x, 1, WorkTable.cursor.position.z), new BABYLON.Vector3(WorkTable.lastSelection.x, 1, WorkTable.lastSelection.z)];
                            }
                        }
                    };
                    var pointerClickAction = function (event) {
                        WorkTable.lastSelection = WorkTable.cursor.position.clone();
                        if (WorkTable.insertModeContext === 'walls' && typeof(WorkTable.cursorHelpers['wallLine']) === 'undefined') {
                            WorkTable.cursorHelpers['wallLine'] = {
                                'lines': [new BABYLON.Vector3(WorkTable.cursor.position.x, 1, WorkTable.cursor.position.z), new BABYLON.Vector3(WorkTable.lastSelection.x + 5, 1, WorkTable.lastSelection.z)]
                            };
                            WorkTable.cursorHelpers['wallLine']['object'] = BABYLON.Mesh.CreateLines('lines', WorkTable.cursorHelpers['wallLine']['lines'], WorkTable.scene, true);
                            WorkTable.cursorHelpers['wallLine']['object'].material = WorkTable.materials.red;
                            console.log(WorkTable.cursorHelpers);
                        }


                        var box = BABYLON.Mesh.CreateBox("box", 0.5, WorkTable.scene);
                        box.position.x = WorkTable.cursor.position.x;
                        box.position.y = 0.25;
                        box.position.z = WorkTable.cursor.position.z;
                        var boxColor = new BABYLON.StandardMaterial('box', WorkTable.scene);
                        boxColor.specular = new BABYLON.Color3(0, 0, 0);
                        boxColor.color = new BABYLON.Color3(.8, .8, .8);
                        box.material = boxColor;
                    };

                    if (this.insertMode) {
                        this.canvas.addEventListener('mousemove', pointerMoveAction);
                        this.registerClickEvent(pointerClickAction);
                        this.cursor = BABYLON.Mesh.CreatePlane("cursor", 0.5, this.scene);
                        this.cursor.position.y = .001;
                        this.cursor.rotation.x = Math.PI / 2;
                        var cursorMaterial = new BABYLON.StandardMaterial('cursor', this.scene);
                        cursorMaterial.diffuseColor = new BABYLON.Color3(.5, .5, .5);
                        this.cursor.material = cursorMaterial;
                    } else {
                        this.canvas.removeEventListener('mousemove', pointerMoveAction);
                        this.unregisterClickEvent(pointerClickAction);
                        this.cursor.dispose();
                    }
                    break;
            }

        }
    };
    var WorkTableGlobals = {
        changePerspective: function (that) {
            if (that.checked) {
                WorkTable.setPerspective(BABYLON.Camera.PERSPECTIVE_CAMERA);
                WorkTable.camera.upperRadiusLimit += WorkTable.cameraRadiusDiff;
                WorkTable.camera.radius += WorkTable.cameraRadiusDiff;
            } else {
                WorkTable.setPerspective(BABYLON.Camera.ORTHOGRAPHIC_CAMERA);
                WorkTable.camera.radius -= WorkTable.cameraRadiusDiff;
                WorkTable.camera.upperRadiusLimit -= WorkTable.cameraRadiusDiff;
            }
            $('#zoom').attr('min', WorkTable.camera.lowerRadiusLimit);
            $('#zoom').attr('max', WorkTable.camera.upperRadiusLimit);
            WorkTable.onZoom();
        },
        moveCamera: function (that, axis, value) {
            var interval = setInterval(function () {
                WorkTable.camera.target[axis] += (.2 * value);
            }, 10);
            $(that).mouseup(function () {
                clearInterval(interval);
            });
            $(that).mouseout(function () {
                clearInterval(interval);
            })
        },
        rotateCamera: function (that, axis, value) {
            var interval = setInterval(function () {
                WorkTable.camera[axis] += (.01 * value);
            }, 10);
            $(that).mouseup(function () {
                clearInterval(interval);
            });
            $(that).mouseout(function () {
                clearInterval(interval);
            })
        },
        setZoom: function (value) {
            WorkTable.camera.radius = Number(value);
        },
        setCameraPosition: function (alpha, beta) {
            WorkTable.camera.target = new BABYLON.Vector3(0, 0, 0);
            WorkTable.camera.alpha = alpha;
            WorkTable.camera.beta = beta;
        },
        setGridPrecision: function (value) {
            WorkTable.gridPrecision = value;
            WorkTable.grid.majorUnitFrequency = WorkTable.gridPrecisionMap[value].gridFrequency;
            WorkTable.grid.gridRatio = WorkTable.gridPrecisionMap[value].gridRatio;
            $('#gridPrecisionValue').html(WorkTable.gridPrecisionMap[value].label);
        },
        changeSnapToGrid: function (that) {
            WorkTable.snapToGrid = that.checked;
        },
        toggleInsertMode: function (that, context) {
            WorkTable.insertMode = that.checked;
            WorkTable.insertModeContext = context;
            WorkTable.toggleMode();
        }
    };
    window['WorkTable'] = WorkTableGlobals;

    $(document).ready(function () {
        WorkTable.init();
        WorkTable.run();
        $(window).resize(function () {
            WorkTable.onResize();
        });
    });
})($);


//# sourceMappingURL=worktable.js.map
