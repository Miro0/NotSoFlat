jQuery.extend(jQuery.validator.messages, {
    required: "Pole wymagane.",
    remote: "Wypełnij to pole.",
    email: "Nieprawidłowy adres email.",
    url: "Nieprawidłowy URL.",
    date: "Nieprawidłowa data.",
    dateISO: "Nieprawidłowy format daty.",
    number: "Podaj liczbę.",
    digits: "Podaj cyfrę.",
    creditcard: "Podaj prawidłową kartę kredytową.",
    equalTo: "Wartości są różne",
    accept: "Podaj wartość z prawidłowym rozszerzeniem.",
    maxlength: jQuery.validator.format("Podaj nie więcej niż {0} znaków."),
    minlength: jQuery.validator.format("Podaj przynajmniej {0} znaków."),
    rangelength: jQuery.validator.format("Podaj wartość o długości od {0} do {1} znaków."),
    range: jQuery.validator.format("Podaj wartość z przedziału od {0} do {1}."),
    max: jQuery.validator.format("Podaj wartość mniejszej bądź równej {0}."),
    min: jQuery.validator.format("Podaj wartość większej bądź równej {0}.")
});

jQuery.validator.addMethod('notEqualTo', function (value, element, param) {
    return this.optional(element) || value != param;
}, 'Nieprawidłowa wartość');

var submitHandlerFunction = function (form) {
    JSUtils.disableElements(form);
    $(form).parents('.ibox-content').addClass('sk-loading');
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        complete: function () {
            $(form).parents('.ibox-content').removeClass('sk-loading');
            JSUtils.enableElements(form);
        },
        success: function (response) {
            var responseType = 'info';
            if (response['type'] !== undefined) {
                responseType = response['type'];
            }
            var redirectTo = response['redirect'];
            swal({
                title: response['title'],
                text: response['text'],
                type: responseType,
                confirmButtonColor: '#74858F',
                confirmButtonText: response['confirmButtonText'],
                closeOnConfirm: redirectTo === undefined,
                allowEscapeKey: redirectTo === undefined
            }, function () {
                if (redirectTo !== undefined) {
                    $('.sweet-alert button').attr('disabled', 'disabled').addClass('disabled');
                    window.location.href = redirectTo;
                }
            });
            if (response['addRules'] !== undefined) {
                for (var i = 0; JSUtils.objectLength(response['addRules']) > i; ++i) {
                    var rule = {};
                    rule[response['addRules'][i]['rule']] = response['addRules'][i]['value'];
                    $(response['addRules'][i]['selector']).rules('add', rule);
                    $(response['addRules'][i]['selector']).focus();
                }
            }
        },
        error: function () {
            swal({
                title: 'Wystąpił błąd',
                type: 'error',
                confirmButtonColor: '#74858F',
                confirmButtonText: 'Zamknij',
                closeOnConfirm: true
            })
        }
    });
};

if ($('#password_confirmation')) {
    $('form').validate({
        rules: {
            password_confirmation: {
                equalTo: '#password'
            }
        },
        messages: {
            password_confirmation: {
                equalTo: 'Hasła nie są identyczne.'
            }
        },
        submitHandler: submitHandlerFunction
    });
} else {
    $('form').validate({
        submitHandler: submitHandlerFunction
    });
}