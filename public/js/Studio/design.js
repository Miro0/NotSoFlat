/**
 * NotSoFlat Studio/design.js
 * 
 * @author: BeedVision
 */

var StudioDesign = {
    canvasId: 'designTable',
    width: 800,
    height: 300,
    gridUnit: 10,
    gridToCentimetersRatio: 50,
    gridBoldColor: '#666',
    gridNormalColor: '#999',
    drawCollisions: false,
    objects: {},
    init: function () {
        if (typeof (designData) === 'object') {
            for (var i = 0; i < JSUtils.objectLength(designData); ++i) {
                var dD = designData[i];
                this.defineObject(dD.type, dD.design_data);
            }
        } else {
            alert('BŁĄD!');
        }
    },
    defineObject: function (type, design_data) {
        this.objects[type] = {};
        var upperCaseName = type.charAt(0).toUpperCase() + type.slice(1);
        this['add' + upperCaseName] = function () {
            StudioDesign.addElementAndObjectDataStructure(type, design_data.name, design_data.shapePointsAmount, design_data.initSize, design_data.layerBase);
        };
    },
    addElementAndObjectDataStructure: function (type, value, shapePointsAmount, initSize, layerBase) {
        var newElement = document.createElement('DIV');
        var newElementId = type + "_" + this.getObjectsAmount(type);
        newElement.id = newElementId;
        newElement.className = type;
        var newElementWidth = initSize * this.gridUnit;
        var newElementHeight = 0;
        if (shapePointsAmount === 2) {
            newElementHeight = this.gridUnit;
        } else {
            newElementHeight = initSize * this.gridUnit;
        }
        newElement.style.width = newElementWidth + 'px';
        newElement.style.height = newElementHeight + 'px';
        newElement.setAttribute('data-type', type);
        newElement.onmousedown = function () {
            StudioDesign.selectElement(this);
        };
        var canvasCoordinates = this.getCanvasCoordinates();
        newElement.style.top = canvasCoordinates.top + 'px';
        newElement.style.left = canvasCoordinates.left + 'px';
        var newObject = this.objects[type][newElementId] = {
            type: type,
            value: value + ' ' + Number(this.getObjectsAmount(type) + 1),
            x: 0,
            y: 0,
            width: 50,
            shapePointsAmount: shapePointsAmount,
            layerBase: layerBase,
            collisions: {},
            collisionState: false,
            constraints: {
                id: {},
                value: {},
                isParent: {},
                relationVector: {}
            },
            lastElementPosition: {
                top: newElement.style.top,
                left: newElement.style.left
            },
        };
        if (shapePointsAmount === 2) {
            newObject.orientation = 'x';
        } else {
            newObject.height = newElementHeight;
            newObject.area = newElementWidth * newElementHeight;
        }
        this.setElementLayer(newElement, 1);

        document.body.appendChild(newElement);

        StudioDesign.selectElement(newElement);
    },
    getElement: function (id) {
        return document.getElementById(id);
    },
    getActiveElement: function () {
        var activeElements = document.getElementsByClassName('active-element');
        if (activeElements.length === 1) {
            return activeElements[0];
        } else {
            return false;
        }
    },
    getObjectForElement: function (element) {
        return this.objects[element.getAttribute('data-type')][element.id];
    },
    getObjectsAmount: function (elementType) {
        if (typeof (this.objects[elementType] !== 'undefined')) {
            return Object.keys(this.objects[elementType]).length;
        } else {
            return false;
        }
    },
    removeElementDraggableAndResiazble: function (element) {
        if ($(element).is('.ui-resizable')) {
            $(element).resizable('destroy');
        }
        if ($(element).is('.ui-draggable')) {
            $(element).draggable('destroy');
        }
    },
    makeElementDraggableAndResizable: function (element) {
        this.removeElementDraggableAndResiazble(element);
        var object = this.getObjectForElement(element);

        $(element).draggable({
            grid: [this.gridUnit, this.gridUnit],
            containment: '#designTable',
            start: function () {
                StudioDesign.calculateElementConstraintRelationVectors(this);
            },
            drag: function () {
                if (element.style.top !== object.lastElementPosition.top || element.style.left !== object.lastElementPosition.left) {
                    if (JSUtils.objectLength(object.constraints.id) > 0) {
                        StudioDesign.moveElementConstraints(element);
                    }
                    StudioDesign.updateElementLastPosition(element);
                    StudioDesign.calculateElementColision(element);
                }
            },
            stop: function () {
                var canvasCoordinates = StudioDesign.getCanvasCoordinates();
                object['x'] = this.offsetLeft - canvasCoordinates.left;
                object['y'] = this.offsetTop - canvasCoordinates.top;
                StudioDesign.moveElementConstraints(this);
                StudioDesign.clearElementConstraintsRelationVector(this);
            }

        });

        var resizeHandles = '';
        var resizeFunction = null;
        if (object.shapePointsAmount === 2) {
            if (object.orientation === 'x') {
                resizeHandles = 'w,e';
            } else {
                resizeHandles = 'n,s';
            }
            resizeFunction = function () {
                if (object.orientation === 'x') {
                    object.width = element.clientWidth;
                } else {
                    object.width = element.clientHeight;
                }
                document.getElementById('objectSize').value = StudioDesign.formatSize(object.width);
                StudioDesign.calculateElementColision(element);
            };
        } else {
            resizeHandles = 'n,s,w,e,ne,nw,se,sw';
            resizeFunction = function () {
                object.width = element.clientWidth;
                object.height = element.clientHeight;
                object.area = Number(object.width * object.height);
                document.getElementById('objectSize').value = StudioDesign.formatSize(object.width, object.height);
                document.getElementById('objectArea').value = StudioDesign.formatArea(object.area);
                StudioDesign.calculateElementColision(element);
            };
        }
        $(element).resizable({
            grid: this.gridUnit,
            handles: resizeHandles,
            resize: resizeFunction
        });
    },
    updateElementLastPosition: function (element) {
        var object = this.getObjectForElement(element);
        object.lastElementPosition = {
            top: element.style.top,
            left: element.style.left
        };
    },
    setElementLayer: function (element, layerNo) {
        element.setAttribute('data-layer', layerNo);
    },
    updateElementLayer: function (element, isActive) {
        var object = this.getObjectForElement(element);
        var layerNo = element.getAttribute('data-layer');
        if (typeof (isActive) === 'undefined') {
            isActive = $(element).hasClass('active-element');
        }
        var diffForActive = 2;
        if (isActive) {
            diffForActive = 1;
        }
        element.style.zIndex = object.layerBase + Number(layerNo * 2) - diffForActive;
    },
    getCanvas: function () {
        var canvas = document.getElementById(this.canvasId);
        var canvasContext = canvas.getContext('2d');
        return canvasContext;
    },
    getCanvasCoordinates: function () {
        var canvas = document.getElementById(this.canvasId);
        return this.getElementCoordinates(canvas);
    },
    getElementCoordinates: function (element) {
        return {
            top: element.offsetTop,
            right: element.offsetLeft + parseInt(element.style.width),
            bottom: element.offsetTop + parseInt(element.style.height),
            left: element.offsetLeft
        };
    },
    parseCoordinate: function (coordinate, width) {
        if (width % 2 !== 0) {
            coordinate += 0.5;
        }
        return coordinate;
    },
    drawLine: function (canvas, x_start, y_start, x_end, y_end, color, width) {
        x_start = this.parseCoordinate(x_start, width);
        x_end = this.parseCoordinate(x_end, width);
        y_start = this.parseCoordinate(y_start, width);
        y_end = this.parseCoordinate(y_end, width);
        canvas.beginPath();
        canvas.moveTo(x_start, y_start);
        canvas.lineTo(x_end, y_end);
        if (typeof (color) === 'undefined') {
            color = '#000';
        }
        canvas.strokeStyle = color;
        if (typeof (width) === 'undefined') {
            width = 1;
        }
        canvas.lineWidth = width;
        canvas.stroke();
        canvas.closePath();
    },
    prepareGrid: function () {
        var canvas = this.getCanvas();
        for (var x = 0; x < this.width; x = x + this.gridUnit) {
            if (x !== 0) {
                this.drawLine(canvas, x, 0, x, this.height, this.gridNormalColor);
            }
        }
        for (var y = 0; y < this.height; y = y + this.gridUnit) {
            if (y !== 0) {
                this.drawLine(canvas, 0, y, this.width, y, this.gridNormalColor);
            }
        }
        for (var x = 0; x < this.width; x = x + 5 * this.gridUnit) {
            if (x !== 0) {
                this.drawLine(canvas, x, 0, x, this.height, this.gridBoldColor, 2);
            }
        }
        for (var y = 0; y < this.height; y = y + 5 * this.gridUnit) {
            if (y !== 0) {
                this.drawLine(canvas, 0, y, this.width, y, this.gridBoldColor, 2);
            }
        }
    },
    selectElement: function (element) {
        if (!$(element).hasClass('active-element')) {
            StudioDesign.clearActiveElements();
            this.makeElementDraggableAndResizable(element);
            $(element).addClass('active-element');
            this.calculateElementColision(element);
            this.selectElementConstraints(element);
            var object = this.getObjectForElement(element);
            this.updateElementLayer(element);

            StudioPanel.showElementDataInActionPanel(element);
            StudioPanel.showObjectConstraintsInPanel(object);
        }
    },
    clearActiveElements: function () {
        var activeElements = document.getElementsByClassName('active-element');
        for (var i = 0; i < activeElements.length; ++i) {
            StudioDesign.removeElementDraggableAndResiazble(activeElements[i]);
            StudioDesign.updateElementLayer(activeElements[i], false);
        }
        if (this.drawCollisions) {
            $('.collision').remove();
        }
        $('.active-element').removeClass('active-element');
    },
    rotateElement: function (element) {
        var object = this.getObjectForElement(element);
        if (object.orientation === 'x') {
            element.style.width = this.gridUnit + 'px';
            element.style.height = object.width + 'px';
            $(element).resizable({handles: 'n,s'});
            object.orientation = 'y';
            this.makeElementDraggableAndResizable(element);
        } else {
            element.style.width = object.width + 'px';
            element.style.height = this.gridUnit + 'px';
            $(element).resizable({handles: 'w,e'});
            object.orientation = 'x';
            this.makeElementDraggableAndResizable(element);
        }
    },
    removeElement: function (element) {
        var objectType = element.getAttribute('data-type');
        var deletingElementIndex = /([0-9]+)$/i.exec(element.id)[1];
        var lastObjectId = objectType + '_' + Number(this.getObjectsAmount(objectType) - 1);
        var isDeletingLastElement = (lastObjectId === element.id);
        delete this.objects[objectType][element.id];
        element.remove();
        if (this.drawCollisions) {
            $('.collision').remove();
        }

        if (this.getObjectsAmount(objectType) > 1) {
            for (var i = deletingElementIndex; i < this.getObjectsAmount(objectType); ++i) {
                var currentElementId = objectType + '_' + i;
                var nextIndex = Number(i) + 1;
                var nextElementId = objectType + '_' + nextIndex;

                this.objects[objectType][currentElementId] = this.objects[objectType][nextElementId];
                if (document.getElementById(nextElementId) !== null) {
                    document.getElementById(nextElementId).id = currentElementId;
                }
            }

            if (!isDeletingLastElement) {
                var lastObjectId = objectType + '_' + Number(this.getObjectsAmount(objectType) - 1);
                delete this.objects[objectType][lastObjectId];
            }
        }
        StudioPanel.setDefaultActionPanel();

    },
    calculateElementColision: function (element) {
        var object = this.getObjectForElement(element);
        var activeElementCoordinates = this.getElementCoordinates(element);
        if (this.drawCollisions) {
            $('.collision').remove();
        }
        object.collisions = {};
        var noCollisionDetected = true;

        for (var i = 0; i < this.getObjectsAmount('room'); ++i) {
            var currentId = 'room_' + i;
            if (currentId !== element.id) {
                var otherElementCoordinates = this.getElementCoordinates(this.getElement(currentId));
                if (activeElementCoordinates.top < otherElementCoordinates.bottom &&
                        activeElementCoordinates.bottom > otherElementCoordinates.top &&
                        activeElementCoordinates.left < otherElementCoordinates.right &&
                        activeElementCoordinates.right > otherElementCoordinates.left) {

                    noCollisionDetected = false;
                    object.collisionState = true;
                    var collisionNumber = Object.keys(object.collisions).length;
                    object.collisions[collisionNumber] = {
                        targetId: currentId,
                        targetValue: this.getObjectForElement(this.getElement(currentId)).value,
                        coordinates: {}
                    };
                    var forCalculationVariablesMap = {
                        0: {
                            /* calculate collision on Y axis*/
                            baseCoordinate: 'top',
                            lengthCoordinate: 'bottom'
                        },
                        1: {
                            /*calculate collision on X axis*/
                            baseCoordinate: 'left',
                            lengthCoordinate: 'right'
                        }

                    };
                    for (var axis = 0; axis < 2; ++axis) {
                        var base = forCalculationVariablesMap[axis].baseCoordinate;
                        var length = forCalculationVariablesMap[axis].lengthCoordinate;
                        if (activeElementCoordinates[base] >= otherElementCoordinates[base]) {
                            object.collisions[collisionNumber].coordinates[base] = Number(activeElementCoordinates[base]);
                            if (activeElementCoordinates[length] >= otherElementCoordinates[length]) {
                                object.collisions[collisionNumber].coordinates[length] = Number(otherElementCoordinates[length]);
                            } else {
                                object.collisions[collisionNumber].coordinates[length] = Number(activeElementCoordinates[length]);
                            }
                        } else {
                            object.collisions[collisionNumber].coordinates[base] = Number(otherElementCoordinates[base]);
                            if (activeElementCoordinates[length] >= otherElementCoordinates[length]) {
                                object.collisions[collisionNumber].coordinates[length] = Number(otherElementCoordinates[length]);
                            } else {
                                object.collisions[collisionNumber].coordinates[length] = Number(activeElementCoordinates[length]);
                            }
                        }
                    }

                    if (this.drawCollisions) {
                        var collisionDiv = document.createElement('DIV');
                        collisionDiv.className = 'collision';
                        collisionDiv.style.top = object.collisions[collisionNumber].coordinates.top + 'px';
                        collisionDiv.style.left = object.collisions[collisionNumber].coordinates.left + 'px';
                        collisionDiv.style.height = Number(object.collisions[collisionNumber].coordinates.bottom - object.collisions[collisionNumber].coordinates.top) + 'px';
                        collisionDiv.style.width = Number(object.collisions[collisionNumber].coordinates.right - object.collisions[collisionNumber].coordinates.left) + 'px';
                        document.body.appendChild(collisionDiv);
                    }
                    this.handleElementCollision(element);
                }
            }
        }
        if (noCollisionDetected && object.collisionState) {
            this.handleElementCollisionRelease(element);
        }
    },
    handleElementCollision: function (element) {
        StudioPanel.clearPanelBody('constraint');
        var object = this.getObjectForElement(element);
        if (Object.keys(object.collisions).length > 0) {
            StudioPanel.showObjectConstraintsInPanel(object);
        }
    },
    handleElementCollisionRelease: function (element) {
        var object = this.getObjectForElement(element);
        StudioPanel.showObjectConstraintsInPanel(object);

        object.collisionState = false;
    },
    addConstraint: function (targetId) {
        var elementsMap = {
            0: this.getActiveElement(),
            1: this.getElement(targetId)
        };
        var activeTargetMap = {
            0: {
                active: 0,
                target: 1
            },
            1: {
                active: 1,
                target: 0
            }
        };
        for (var i = 0; i < JSUtils.objectLength(elementsMap); ++i) {
            var activeObject = this.getObjectForElement(elementsMap[activeTargetMap[i].active]);
            var targetObject = this.getObjectForElement(elementsMap[activeTargetMap[i].target]);

            if (!JSUtils.inObject(elementsMap[activeTargetMap[i].active].id, activeObject.constraints.id)) {
                var activeObjectConstraintIndex = JSUtils.objectLength(activeObject.constraints.id);
                activeObject.constraints.id[activeObjectConstraintIndex] = elementsMap[activeTargetMap[i].target].id;
                activeObject.constraints.value[activeObjectConstraintIndex] = targetObject.value;
                activeObject.constraints.isParent[activeObjectConstraintIndex] = activeObject.type === 'room';
                activeObject.constraints.relationVector[activeObjectConstraintIndex] = {};
            }
        }

        this.selectElementConstraints(elementsMap[0]);
        StudioPanel.showObjectConstraintsInPanel(this.getObjectForElement(elementsMap[activeTargetMap[0].active]));
    },
    removeConstraint: function (targetId) {
        var elementsMap = {
            0: this.getActiveElement(),
            1: this.getElement(targetId)
        };
        var activeTargetMap = {
            0: {
                active: 0,
                target: 1
            },
            1: {
                active: 1,
                target: 0
            }
        };

        for (var i = 0; i < JSUtils.objectLength(elementsMap); ++i) {
            var object = this.getObjectForElement(elementsMap[i]);
            var constraintKey = JSUtils.inObject(elementsMap[activeTargetMap[0].target].id, object.constraints.id, false, true);
            if (constraintKey !== false) {
                object.constraints.id = JSUtils.deleteAndRemoveEmptyKeyFromObject(object.constraints.id, constraintKey);
                object.constraints.value = JSUtils.deleteAndRemoveEmptyKeyFromObject(object.constraints.value, constraintKey);
                object.constraints.isParent = JSUtils.deleteAndRemoveEmptyKeyFromObject(object.constraints.isParent, constraintKey);
                object.constraints.relationVector = JSUtils.deleteAndRemoveEmptyKeyFromObject(object.constraints.relationVector, constraintKey);
            }
        }

        this.selectElementConstraints(elementsMap[0]);
        StudioPanel.showObjectConstraintsInPanel(this.getObjectForElement(elementsMap[activeTargetMap[0].active]));
    },
    selectElementConstraints: function (element) {
        $('.constraint').removeClass('constraint');
        var object = this.getObjectForElement(element);
        for (var i = 0; i < JSUtils.objectLength(object.constraints.id); ++i) {
            var targetElement = this.getElement(object.constraints.id[i]);
            $(targetElement).addClass('constraint');
        }
    },
    calculateElementConstraintRelationVectors: function (element) {
        var object = this.getObjectForElement(element);
        for (var i = 0; i < JSUtils.objectLength(object.constraints.id); ++i) {
            var targetElement = this.getElement(object.constraints.id[i]);
            object.constraints.relationVector[i] = {
                top: parseInt(element.style.top) - parseInt(targetElement.style.top),
                left: parseInt(element.style.left) - parseInt(targetElement.style.left)
            };
        }
    },
    clearElementConstraintsRelationVector: function (element) {
        var object = this.getObjectForElement(element);
        for (var i = 0; i < JSUtils.objectLength(object.constraints.id); ++i) {
            var targetElement = this.getElement(object.constraints.id[i]);
            object.constraints.relationVector[i] = {};
        }
    },
    moveElementConstraints: function (element) {
        var object = this.getObjectForElement(element);
        for (var i = 0; i < JSUtils.objectLength(object.constraints.id); ++i) {
            if (object.constraints.isParent[i]) {
                var targetElement = this.getElement(object.constraints.id[i]);
                targetElement.style.top = Number(parseInt(element.style.top) - object.constraints.relationVector[i].top) + 'px';
                targetElement.style.left = Number(parseInt(element.style.left) - object.constraints.relationVector[i].left) + 'px';
                this.updateElementLastPosition(targetElement);
            }
        }
    },
    formatSize: function (x, y) {
        var returnValue = Number(x / StudioDesign.gridToCentimetersRatio) + ' m';
        if (typeof (y) !== 'undefined') {
            returnValue += ' x ' + Number(y / StudioDesign.gridToCentimetersRatio) + ' m';
        }
        return returnValue;
    },
    formatArea: function (x) {
        return Number(x / (StudioDesign.gridToCentimetersRatio * StudioDesign.gridToCentimetersRatio)) + ' m2';
    },
    parseDataToRenderer: function (url) {
        var dataForm = document.createElement('FORM');
        dataForm.setAttribute('method', "post");
        dataForm.setAttribute('action', url);
        var data = document.createElement('TEXTAREA');
        data.name = 'data';
        data.id = 'data';
        data.value = JSON.stringify(StudioDesign.objects);
        dataForm.appendChild(data);
        var token = document.getElementsByName('_token')[0];
        dataForm.appendChild(token);
        dataForm.submit();
    }


};
StudioDesign.init();
StudioDesign.prepareGrid();



