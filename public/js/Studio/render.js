/**
 * NotSoFlat Studio/render.js
 * 
 * @author: BeedVision
 */

var StudioRenderer = {
    rendererId: 'rendererCanvas',
    isValid: true,
    unitScaleRatio: 10,
    init: function () {
        if (renderData === null) {
            this.isValid = false;
        } else {
            document.getElementById('rendererWrapper').style.display = 'block';
        }
    },
    renderScene: function (data) {
        var canvas = document.getElementById(this.rendererId);
        var engine = new BABYLON.Engine(canvas, true);
        var scene = new BABYLON.Scene(engine);
        scene.clearColor = new BABYLON.Color3(1, 1, 1);
        var camera = new BABYLON.FreeCamera("camera", new BABYLON.Vector3(40, -15, -50), scene);
        camera.setTarget(new BABYLON.Vector3(40, -15, 0));
        camera.attachControl(canvas, false);
        var light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), scene);
        light.intensity = .5;

/* Render rooms */
        var rooms = new Array();
        for (var i = 0; i < Object.keys(renderData['room']).length; ++i) {
            var roomId = 'room_' + i;
            rooms[i] = BABYLON.Mesh.CreatePlane(roomId, 1, scene);
            
            rooms[i].position.x = renderData.room[roomId]['x'] / this.unitScaleRatio + (renderData.room[roomId]['width'] / this.unitScaleRatio / 2);
            rooms[i].position.y = -renderData.room[roomId]['y'] / this.unitScaleRatio - (renderData.room[roomId]['height'] / this.unitScaleRatio / 2);
            rooms[i].scaling.x = renderData.room[roomId]['width'] / this.unitScaleRatio;
            rooms[i].scaling.y = renderData.room[roomId]['height'] / this.unitScaleRatio;
        }        

        engine.runRenderLoop(function () {
            scene.render();
        });
    }
};

StudioRenderer.init();
if (StudioRenderer.isValid) {
    StudioRenderer.renderScene(renderData);
}




