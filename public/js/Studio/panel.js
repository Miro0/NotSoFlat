/**
 * NotSoFlat Studio/panel.js
 * 
 * @author: BeedVision
 */

var StudioPanel = {
    icon: function (name) {
        return '<i class="glyphicon glyphicon-' + name + '"></i>';
    },
    createBasicElement: function (elementType, className, name, html, value) {
        var element = document.createElement(elementType);
        element.className = className;
        if (typeof (name) !== 'undefined') {
            element.id = name;
            element.name = name;
        }
        if (typeof (html) !== 'undefined') {
            element.innerHTML = html;
        }
        if (typeof (value) !== 'undefined') {
            element.value = value;
        }
        return element;
    },
    getInlineEditInput: function (name, label, value, onConfirm) {
        var mainWrapper = StudioPanel.createBasicElement('DIV', 'input-group');
        var labelElement = StudioPanel.createBasicElement('SPAN', 'input-group-addon label-primary', 'label', label);
        var input = StudioPanel.createBasicElement('INPUT', 'form-control', name, null, value);
        input.readOnly = true;
        input.setAttribute('data-value', value);
        input.onkeypress = function (event) {
            if (!event) {
                event = window.event;
            }
            var keyCode = event.keyCode || event.which;
            if (keyCode == '13') {
                alert('CONFIRM');
                return false;
            }
        };
        var buttonGroup = StudioPanel.createBasicElement('DIV', 'input-group-btn', 'buttonGroup');
        var editButton = StudioPanel.createBasicElement('BUTTON', 'btn btn-default', 'editButton', StudioPanel.icon('pencil'));
        editButton.onclick = function () {
            input.readOnly = false;
            input.focus();
            $(editButton).addClass('hidden');
            $(confirmButton).removeClass('hidden');
            $(cancelButton).removeClass('hidden');
        };
        var confirmButton = StudioPanel.createBasicElement('BUTTON', 'btn btn-success hidden', 'confirmButton', StudioPanel.icon('ok'));
        var confirmFunction = function () {
            input.readOnly = true;
            $(editButton).removeClass('hidden');
            $(confirmButton).addClass('hidden');
            $(cancelButton).addClass('hidden');
            input.setAttribute('data-value', input.value);
            onConfirm(input.value);
        };
        confirmButton.onclick = confirmFunction;
        var cancelButton = StudioPanel.createBasicElement('BUTTON', 'btn btn-danger hidden', 'cancelButton', StudioPanel.icon('remove'));
        var cancelFunction = function () {
            input.readOnly = true;
            $(editButton).removeClass('hidden');
            $(confirmButton).addClass('hidden');
            $(cancelButton).addClass('hidden');
            input.value = input.getAttribute('data-value');
        };
        cancelButton.onclick = cancelFunction;
        input.onkeypress = function (event) {
            if (!this.readOnly) {
                if (!event) {
                    event = window.event;
                }
                var keyCode = event.keyCode || event.which;
                if (keyCode == '13') {
                    confirmFunction();
                    return false;
                }
            }
        };
        input.onkeyup = function (event) {
            if (!this.readOnly) {
                if (!event) {
                    event = window.event;
                }
                var keyCode = event.keyCode || event.which;
                if (keyCode == '27') {
                    cancelFunction();
                    return false;
                }
            }
        };


        mainWrapper.appendChild(labelElement);
        mainWrapper.appendChild(input);
        buttonGroup.appendChild(editButton);
        buttonGroup.appendChild(confirmButton);
        buttonGroup.appendChild(cancelButton);
        mainWrapper.appendChild(buttonGroup);

        return mainWrapper;
    },
    getDataField: function (name, label, value) {
        var mainWrapper = StudioPanel.createBasicElement('DIV', 'input-group');
        var labelElement = StudioPanel.createBasicElement('SPAN', 'input-group-addon label-info', 'label', label);
        var input = StudioPanel.createBasicElement('INPUT', 'form-control', name, null, value);
        input.readOnly = true;
        input.setAttribute('data-value', value);

        mainWrapper.appendChild(labelElement);
        mainWrapper.appendChild(input);

        return mainWrapper;
    },
    getButton: function (name, label, className, icon, onclickFunction) {
        var button = StudioPanel.createBasicElement('BUTTON', 'btn btn-default ' + className, name, StudioPanel.icon(icon) + ' ' + label);
        if (typeof (onclickFunction) === 'function') {
            button.onclick = function () {
                onclickFunction();
            };
        } else {
            button.setAttribute('onclick', onclickFunction);
        }
        return button;
    },
    getRange: function (name, label, value, minValue, maxValue, onchangeFunction) {
        var mainWrapper = StudioPanel.createBasicElement('DIV', 'input-group');
        var labelElement = StudioPanel.createBasicElement('SPAN', 'input-group-addon label-success', 'label', label);
        var input = StudioPanel.createBasicElement('INPUT', 'form-control', name, null, value);
        input.readOnly = true;
        var buttonGroup = StudioPanel.createBasicElement('DIV', 'input-group-btn', 'buttonGroup');
        var inputChangeFunction = function (input, newValue) {
            if (Number(input.value) === minValue) {
                $(minusButton).addClass('disabled');
            } else if ($(minusButton).hasClass('disabled')) {
                $(minusButton).removeClass('disabled');
            }
            if (Number(input.value) === maxValue) {
                $(plusButton).addClass('disabled');
            } else if ($(plusButton).hasClass('disabled')) {
                $(plusButton).removeClass('disabled');
            }
            onchangeFunction(newValue);
        };
        var plusButton = StudioPanel.createBasicElement('BUTTON', 'btn btn-default', 'plusButton', StudioPanel.icon('plus'));
        plusButton.onclick = function () {
            var newValue = input.value = Number(input.value) + 1;
            inputChangeFunction(input, newValue);
        };
        var minusButton = StudioPanel.createBasicElement('BUTTON', 'btn btn-default', 'minusButton', StudioPanel.icon('minus'));
        minusButton.onclick = function () {
            var newValue = input.value = Number(input.value) - 1;
            inputChangeFunction(input, newValue);
        };
        inputChangeFunction(input, value);

        mainWrapper.appendChild(labelElement);
        mainWrapper.appendChild(input);
        buttonGroup.appendChild(plusButton);
        buttonGroup.appendChild(minusButton);
        mainWrapper.appendChild(buttonGroup);

        return mainWrapper;
    },
    showElementDataInActionPanel: function (element) {
        var object = StudioDesign.getObjectForElement(element);
        var body = this.createBasicElement('DIV', 'panelBody');
        switch (object.shapePointsAmount) {
            case 4:
                body.appendChild(this.getInlineEditInput('objectType', 'Nazwa', object.value, function (result) {
                    object.value = result;
                }));
                body.appendChild(this.getDataField('objectSize', 'Wymiary', StudioDesign.formatSize(object.width, object.height)));
                body.appendChild(this.getDataField('objectArea', 'Powierzchnia', StudioDesign.formatArea(object.area)));
                body.appendChild(this.getRange('objectLayer', 'Warstwa', element.getAttribute('data-layer'), 1, 5, function (layerNo) {
                    StudioDesign.setElementLayer(element, layerNo);
                    StudioDesign.updateElementLayer(element);
                }));
                body.appendChild(this.getButton('objectDelete', 'Usuń', 'btn-danger', 'remove', function () {
                    StudioDesign.removeElement(element);
                }));
                break;
            case 2:
                body.appendChild(this.getInlineEditInput('objectType', 'Nazwa', object.value, function (result) {
                    object.value = result;
                }));
                body.appendChild(this.getDataField('objectSize', 'Wymiary', StudioDesign.formatSize(object.width)));
                body.appendChild(this.getButton('objectOrientation', 'Obróć', null, 'repeat', function () {
                    StudioDesign.rotateElement(element);
                }));
                body.appendChild(this.getButton('objectDelete', 'Usuń', 'btn-danger', 'remove', function () {
                    StudioDesign.removeElement(element);
                }));
                break;
            default:
                head = 'Błąd danych';
                body = this.createBasicElement('DIV', 'panel-danger', 'error', 'Wybrano nieznany lub błędny element');
        }

        this.updatePanel('action', 'Edytuj objekt', body);
    },
    showObjectConstraintsInPanel: function (object) {
        var body = this.createBasicElement('DIV', 'panelBody');
        var foundConstraint = false;
        for (var i = 0; i < JSUtils.objectLength(object.constraints.id); ++i) {
            var name = 'removeConstraint_' + i;
            var label = 'Usuń wiązanie z: <strong>' + object.constraints.value[i] + '</strong>';
            var targetId = object.constraints.id[i];
            body.appendChild(this.getButton(name, label, 'btn-info btn-new-line', 'link', 'StudioDesign.removeConstraint(\'' + targetId + '\');'));
            foundConstraint = true;
        }
        for (var i = 0; i < JSUtils.objectLength(object.collisions); ++i) {
            if (!JSUtils.inObject(object.collisions[i].targetId, object.constraints.id)) {
                var name = 'addConstraint_' + i;
                var label = 'Powiąż z: <strong>' + object.collisions[i].targetValue + '</strong>';
                var targetId = object.collisions[i].targetId;
                body.appendChild(this.getButton(name, label, 'btn-success btn-new-line', 'link', 'StudioDesign.addConstraint(\'' + targetId + '\');'));
                foundConstraint = true;
            }
        }
        if (foundConstraint) {
            this.updatePanel('constraint', 'Wiązania z pokojami', body);
        } else {
            this.hidePanel('constraint');
        }
    },
    setDefaultActionPanel: function () {
        var head = 'Okno Akcji';
        var body = this.createBasicElement('DIV', 'panel-info', 'success', 'Dodawaj lub edytuj elementy w mieszkaniu');
        this.updatePanel('action', head, body);
    },
    getPanel: function (panelType) {
        return {
            wrapper: document.getElementById(panelType + 'PanelWrapper'),
            head: document.getElementById(panelType + 'PanelHead'),
            body: document.getElementById(panelType + 'PanelBody')
        };
    },
    updatePanel: function (panelType, head, body) {
        var panel = this.getPanel(panelType);
        panel.head.innerHTML = head;
        this.clearPanelBody(panelType);
        panel.body.appendChild(body);
        if ($(panel.wrapper).hasClass('hidden')) {
            $(panel.wrapper).removeClass('hidden');
        }
    },
    clearPanelBody: function (panelType) {
        var panel = this.getPanel(panelType);
        while (panel.body.firstChild) {
            panel.body.removeChild(panel.body.firstChild);
        }
    },
    hidePanel: function (panelType) {
        var panel = this.getPanel(panelType);
        if (!$(panel.wrapper).hasClass('hidden')) {
            $(panel.wrapper).addClass('hidden');
        }
    }
};

StudioPanel.setDefaultActionPanel();
