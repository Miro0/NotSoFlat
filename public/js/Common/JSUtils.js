/**
 * NotSoFlat Common/JSUtils.js
 *
 * @author: BeedVision
 */

var JSUtils = {
    objectLength: function (object) {
        return Object.keys(object).length;
    },
    inObject: function (needle, haystack, searchRecursively, returnKeyValue) {
        var found = false;
        if (typeof (searchRecursively) === 'undefined') {
            searchRecursively = false;
        }
        if (typeof (returnKeyValue) === 'undefined') {
            returnKeyValue = false;
        }
        var keys = Object.keys(haystack);
        for (var i = 0; i < this.objectLength(haystack); ++i) {
            if (haystack[keys[i]] === needle) {
                if (returnKeyValue) {
                    found = keys[i];
                } else {
                    found = true;
                }
            } else if (searchRecursively && typeof (haystack[keys[i]]) === 'object') {
                found = JSUtils.inObject(needle, haystack[keys[i]]);
            }
        }
        return found;
    },
    removeEmptyKeysFromObject: function (object) {
        var returnObject = {};
        var returnObjectIndex = 0;
        for (var i = 0; i < (this.objectLength(object) + 1); ++i) {
            if (typeof (object[i]) === 'undefined') {
                continue;
            } else {
                if (typeof (object[i]) !== undefined) {
                    returnObject[returnObjectIndex] = object[i];
                    ++returnObjectIndex;
                }
            }
        }

        return returnObject;
    },
    deleteAndRemoveEmptyKeyFromObject: function (object, key) {
        delete object[key];
        var newObject = this.removeEmptyKeysFromObject(object);
        return newObject;
    },
    disableElement: function (button) {
        $(button).addClass('disabled').attr('disabled', 'disabled');
    },
    enableElement: function (button) {
        $(button).attr('disabled', false).removeClass('disabled');
    },
    disableElements: function(form) {
        this.disableElement($(form).find('button'));
        this.disableElement($(form).find('icheckbox_square-green'));
    },
    enableElements: function(form) {
        this.enableElement($(form).find('button'));
        this.enableElement($(form).find('icheckbox_square-green'));
    }
};
