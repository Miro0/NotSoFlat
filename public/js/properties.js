$('.toolbar-wrapper').find('a').click(function () {
    $('.toolbar-wrapper').addClass('show');
});

$('.worktable').click(function () {
    $('.toolbar-wrapper').removeClass('show');
    setTimeout(function () {
        $('.toolbar-wrapper .active').removeClass('active');
    }, 500);
});
window.addEventListener('DOMContentLoaded', function () {
    var canvas = document.getElementById('worktable');
    var engine = new BABYLON.Engine(canvas, true);
    var camera = null;
    var worktableRatio = $('#worktable').width() / $('#worktable').height();
    var createScene = function () {
        var scene = new BABYLON.Scene(engine);
        scene.clearColor = BABYLON.Color3.White();
        scene.fogMode = BABYLON.Scene.FOGMODE_LINEAR;
        scene.fogColor = BABYLON.Color3.White();
        scene.fogStart = 60.0;
        scene.fogEnd = 100.0;

        camera = new BABYLON.ArcRotateCamera("camera1", 0, 0, 25, new BABYLON.Vector3(0, 0, 0), scene);
        camera.setTarget(new BABYLON.Vector3(0, 0, 0));
        camera.checkCollisions = true;
        camera.lowerBetaLimit = 0;
        camera.upperBetaLimit = Math.PI - 1.6;
        camera.lowerRadiusLimit = 5;
        camera.upperRadiusLimit = 100;


        camera.mode = BABYLON.Camera.ORTHOGRAPHIC_CAMERA;
        camera.orthoTop = 20;
        camera.orthoBottom = -20;
        camera.orthoLeft = -20;
        camera.orthoRight = 20;

        camera.panningSensibility = 100;
        camera.attachControl(canvas, false, false, 0);

        var ground = BABYLON.Mesh.CreateGround('ground', 240, 240, 1, scene);

        var grid = new BABYLON.GridMaterial("groundMaterial", scene);
        // grid.opacity = 0.99;
        grid.gridRatio = 1;
        grid.minorUnitVisibility = .05;

        ground.material = grid;
        ground.checkCollisions = true;

        var red = new BABYLON.StandardMaterial('red', scene);
        red.emissiveColor = new BABYLON.Color3(1, 0, 0);
        red.diffuseColor = new BABYLON.Color3(.8, 0, 0);

        var commonOnPointerOverFunction = function (objectMesh) {
            if (objectMesh.meshUnderPointer !== null) {
                box.customOutline = BABYLON.Mesh.CreateBox("BoxOutline", 1.1, scene, false, BABYLON.Mesh.BACKSIDE);
                box.customOutline.parent = box;
                box.customOutline.material = red;
                box.customOutline.isPickable = false;
            }
        };
        var commonOnPointerOutFunction = function (objectMesh) {
            box.customOutline.dispose();
        };

        var startingPoint;
        var currentMesh;
        var getGroundPosition = function () {
            var pickinfo = scene.pick(scene.pointerX, scene.pointerY, function (mesh) { return mesh == ground; });
            if (pickinfo.hit) {
                return pickinfo.pickedPoint;
            }
            return null;
        };


        var commonOnPointerDownFunction = function (event) {
            if (event.button === 0) {
                var pickInfo = scene.pick(scene.pointerX, scene.pointerY, function (mesh) {
                    return mesh !== ground && mesh.isPickable;
                });
                if (pickInfo.hit) {
                    console.log(pickInfo.pickedMesh.children);
                    currentMesh = pickInfo.pickedMesh;
                    startingPoint = getGroundPosition();
                    if (startingPoint) {
                        setTimeout(function () {
                            camera.detachControl(canvas);
                        }, 0);
                    }
                }
            }
        };
        var commonOnPointerUpFunction = function () {
            if (startingPoint) {
                camera.attachControl(canvas, false, false, 0);
                startingPoint = null;
                return;
            }
        };
        var commonOnPointerMoveFunction = function (event) {
            if (!startingPoint) {
                return;
            }
            var current = getGroundPosition();
            if (!current) {
                return;
            }
            var diff = current.subtract(startingPoint);
            currentMesh.position.addInPlace(diff);
            startingPoint = current;
        };
        canvas.addEventListener("pointerdown", commonOnPointerDownFunction, false);
        canvas.addEventListener("pointerup", commonOnPointerUpFunction, false);
        canvas.addEventListener("pointermove", commonOnPointerMoveFunction, false);

        var box = BABYLON.Mesh.CreateBox('textBox', 1, scene, true);
        box.position = new BABYLON.Vector3(.5, .5, .5);
        box.actionManager = new BABYLON.ActionManager(scene);
        var boxOnPointerOverAction = new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOverTrigger, commonOnPointerOverFunction);
        var boxOnPointerOutAction = new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPointerOutTrigger, commonOnPointerOutFunction);
        box.actionManager.registerAction(boxOnPointerOverAction);
        box.actionManager.registerAction(boxOnPointerOutAction);


        var disc = BABYLON.MeshBuilder.CreateDisc("disc", {tessellation: 32, size: 1}, scene);
        disc.position.y = 1.1;
        disc.rotation.x = Math.PI / 2;
        disc.material = red;

        return scene;
    }

    var scene = createScene();
    engine.runRenderLoop(function () {
        if (camera.beta === 0) {
            camera.panningAxis = new BABYLON.Vector3(1, 1, 0);
        } else {
            camera.panningAxis = new BABYLON.Vector3(1, 0, 1);
        }

        camera.orthoTop = camera.radius;
        camera.orthoBottom = -camera.radius;
        camera.orthoRight = camera.radius * worktableRatio;
        camera.orthoLeft = -camera.radius * worktableRatio;

        scene.render();
    });
    window.addEventListener('resize', function () {
        engine.resize();
        worktableRatio = $('#worktable').width() / $('#worktable').height();
    });
});

$(window).resize(function () {
    $('.worktable').css('height', $(window).height() - 191);
}).resize();


//# sourceMappingURL=properties.js.map
