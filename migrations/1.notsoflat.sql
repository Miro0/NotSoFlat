-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 29 Sty 2017, 16:27
-- Wersja serwera: 5.7.14
-- Wersja PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `notsoflat`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2016_05_26_181915_create_objects_table', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `objects`
--

CREATE TABLE `objects` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(30) NOT NULL,
  `design_data` varchar(600) NOT NULL,
  `render_data` varchar(600) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `objects`
--

INSERT INTO `objects` (`id`, `type`, `design_data`, `render_data`, `created_at`, `updated_at`) VALUES
(1, 'room', 'a:6:{s:4:"name";s:6:"Pokój";s:17:"shapePointsAmount";i:4;s:8:"initSize";i:10;s:9:"layerBase";i:10;s:9:"minHeight";i:2;s:9:"maxHeight";i:5;}', 'todo', '2016-05-27 16:38:30', '2016-06-03 19:32:00'),
(2, 'window', 'a:7:{s:4:"name";s:4:"Okno";s:17:"shapePointsAmount";i:2;s:8:"initSize";i:5;s:9:"layerBase";i:20;s:9:"minHeight";i:1;s:9:"maxHeight";i:5;s:13:"isDirectional";s:2:"on";}', 'todo', '2016-05-27 16:42:06', '2016-06-03 19:33:21'),
(3, 'door', 'a:7:{s:4:"name";s:5:"Drzwi";s:17:"shapePointsAmount";i:2;s:8:"initSize";i:5;s:9:"layerBase";i:30;s:9:"minHeight";i:2;s:9:"maxHeight";i:5;s:13:"isDirectional";s:2:"on";}', 'todo', '2016-05-27 16:44:29', '2016-06-03 19:32:06'),
(6, 'table', 'a:6:{s:4:"name";s:6:"Stół";s:17:"shapePointsAmount";i:4;s:8:"initSize";i:2;s:9:"layerBase";i:20;s:9:"minHeight";i:1;s:9:"maxHeight";i:2;}', 'todo', '2016-06-03 19:33:59', '2016-06-03 19:33:59');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` enum('admin','premium','free') NOT NULL DEFAULT 'free',
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, '2@o2.pl', '$2y$10$u1hy9HSD6Fr0No3AVFHglOfPMCl9/inQ79VwCpemrS6CRCamIwwJu', 'free', 'GI8IObxZFWpDlOHMRVcOyQdGhmFvp1UmiagRsphpSuEfJu5r8jb1mpj4iM3G', '2017-01-28 21:23:12', '2017-01-29 15:19:29'),
(2, NULL, 'a@o2.pl', '$2y$10$fILh0n2W6DgvOwff/JxQW.hjTG.l3LJ14V.ltSn.49hkko7lYIgDy', 'free', NULL, '2017-01-28 21:40:26', '2017-01-28 21:40:26'),
(3, NULL, '2@o2.pl2', '$2y$10$ABpnj/dgqXXcUnorLeQTk.Zf8sF5uicf0mgotTG//tpiCtkRiCt0m', 'free', NULL, '2017-01-28 22:44:00', '2017-01-28 22:44:00'),
(4, NULL, '2@o2.p', '$2y$10$KD6LpOdeaTX.YD/7y3xcQuau06NpiU4AnK/AB5bgW2VFsyRlXPufK', 'free', NULL, '2017-01-28 22:44:18', '2017-01-28 22:44:18'),
(5, NULL, 'mirogubala@gmail.com', '$2y$10$XUZ/kwDhM5lwPkdalRnTReGyq3hgtkIQWurMiJx0Di0jltTQ1vTce', 'free', NULL, '2017-01-29 14:40:33', '2017-01-29 14:40:33'),
(6, NULL, 'mir@o2.pl', '$2y$10$JJFIfDma78tDkZOMxT5.K.fzIVxwxWDMfyGDs/axseedQ59r08HjO', 'free', NULL, '2017-01-29 14:42:09', '2017-01-29 14:42:09'),
(7, NULL, 'mirrr@o2.pl', '$2y$10$/PdKldGYaOW00nvPKHHAMeSsjitRO0LMIz/nSfJ6dv4RXPt5EcJEO', 'free', '1ROmEzscEA0QYNOBsdmcWCO0WcPE6bgaNEhHQBwI3rv2vgnl8p6Bu12Dn8VV', '2017-01-29 14:44:36', '2017-01-29 14:50:12');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `objects`
--
ALTER TABLE `objects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
