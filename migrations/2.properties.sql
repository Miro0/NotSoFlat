CREATE TABLE `properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `city` varchar (150) NOT NULL,
  `street` varchar (150) NOT NULL,
  `area` FLOAT NOT NULL,
  `year` INT(4),
  `window` ENUM('D','P') COMMENT 'Drewniane,Plastic',
  `heating` ENUM('CO', 'E') COMMENT 'CO,Elektryczne',
  `parking` ENUM('G','U') COMMENT ',Garaż,Ulica',
  `type` ENUM('F','H') COMMENT 'Flat,Home',


  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES Users(id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `auctions` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL,
  `type` ENUM('S','R') NOT NULL COMMENT 'Sell,Rent',
  `main_price` int (10) NOT NULL,
  `market` ENUM('P', 'S') NOT NULL COMMENT 'Primary,Secondary',
  `from` ENUM('O', 'A') COMMENT 'Owner,Agent',



  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`property_id`) REFERENCES Properties(id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;