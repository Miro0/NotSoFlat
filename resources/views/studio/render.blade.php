@extends('layouts.app')

@section('styles')
{{ HTML::style('css/Studio/render.css') }}
@endsection

@section('scripts')
{{ HTML::script('js/babylon.js') }}
{{ HTML::script('js/hand-1.3.7.js') }}
{{ HTML::script('js/Studio/render.js') }}
@endsection

@section('content')

<script>
    var renderData = <?= $renderData ?>;
</script>

<div id="rendererWrapper" class="jumbotron" style="display: none;">
    <canvas id="rendererCanvas" class="renderer"></canvas>
</div>

@endsection

