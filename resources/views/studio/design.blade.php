@extends('layouts.app')

@section('styles')
{{ HTML::style('css/Studio/design.css') }}
{{ HTML::style('css/Studio/panel.css') }}
@endsection

@section('scripts')
{{ HTML::script('js/Studio/design.js') }}
{{ HTML::script('js/Studio/panel.js') }}
@endsection

@section('content')
<script>
    var designData = <?= $json ?>;
</script>

<div class="leftPanel">
    <canvas id="designTable" width="600" height="300">
    </canvas>

    <br />
    <div class="btn-group" >
        <?php foreach($data as $object) :?>
        <button class="btn btn-primary" onclick="StudioDesign.add<?= ucfirst($object['type']) ?>();">Dodaj <?= lcfirst($object['value']) ?></button>
        <?php endforeach; ?>
        {{ csrf_field() }}
        <button class="btn btn-success" onclick="StudioDesign.parseDataToRenderer('{{ url("studio/render") }}');">Pokaż wizualizację</button>
    </div>
</div>

<div class="right-panels">
    <div id="actionPanelWrapper" class="panel panel-default">
        <div id="actionPanelHead" class="panel-heading"></div>
        <div id="actionPanelBody" class="panel-body"></div>
    </div>

    <div id="constraintPanelWrapper" class="panel panel-default hidden">
        <div id="constraintPanelHead" class="panel-heading">Powiązania</div>
        <div id="constraintPanelBody" class="panel-body"></div>
    </div>
</div>



@endsection


