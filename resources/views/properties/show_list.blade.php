@extends('layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading animated fadeInDown">
        <div class="col-xs-12 ">

            <div class="pull-right">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                        class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="">
                            <a aria-expanded="false" role="button" href="{{ url('/properties/add') }}"><i class="fa fa-plus-circle"></i> Dodaj nieruchomość</a>
                        </li>
                    </ul>
                </div>
            </div>

            <h2><i class="fa fa-home"></i> Moje nieruchomości</h2>
        </div>
    </div>

    @include('paginator.table', ['columns' => ['name','created_at'], 'rows' => [['Miro','now']]])

@endsection