@extends('layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading over-worktable">
        <div class="col-xs-12 ">

            <div class="pull-right">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                        class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="">
                            <a aria-expanded="false" role="button" href="{{ url('/') }}"> Test guzik</a>
                        </li>
                    </ul>
                </div>
            </div>

            <h2><i class="fa fa-home"></i> Nowa nieruchomość</h2>
        </div>
    </div>

    <canvas class="worktable workarea" id="worktable"></canvas>

    <div class="toolbar-wrapper workarea toolbar-worktable show">
        <div class="ibox collapsed">
            <div class="ibox-title collapse-link">
                <i class="fa fa-chevron-up"></i> Nawigacja
            </div>
            <div class="ibox-content">
                <div class="form-group">
                    <div class="arrow-wrapper">
                        <div class="arrow-container">
                            <div class="arrow up" onmousedown="WorkTable.moveCamera(this, 'x', -1);"><i class="fa fa-sort-up"></i></div>
                            <div class="arrow down" onmousedown="WorkTable.moveCamera(this, 'x', 1);"><i class="fa fa-sort-up"></i></div>
                            <div class="arrow left" onmousedown="WorkTable.moveCamera(this, 'z', -1);"><i class="fa fa-sort-up"></i></div>
                            <div class="arrow right" onmousedown="WorkTable.moveCamera(this, 'z', 1);"><i class="fa fa-sort-up"></i></div>
                        </div>
                        <div class="arrow-caption">
                            Ruch
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="arrow-wrapper">
                        <div class="arrow-container">
                            <div class="arrow up" onmousedown="WorkTable.rotateCamera(this, 'beta', -1);"><i class="fa fa-angle-double-up"></i></div>
                            <div class="arrow down" onmousedown="WorkTable.rotateCamera(this, 'beta', 1);"><i class="fa fa-angle-double-up"></i></div>
                            <div class="arrow left" onmousedown="WorkTable.rotateCamera(this, 'alpha', -1);"><i class="fa fa-angle-double-up"></i></div>
                            <div class="arrow right" onmousedown="WorkTable.rotateCamera(this, 'alpha', 1);"><i class="fa fa-angle-double-up"></i></div>
                        </div>
                        <div class="arrow-caption">
                            Obrót
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="range-wrapper">
                        <input name="zoom" id="zoom" type="range" min="2" max="50" step=".1" oninput="WorkTable.setZoom(this.value);" onchange="WorkTable.setZoom(this.value);"/>
                        <i class="fa fa-plus-circle"></i> Przybliżenie <i class="fa fa-minus-circle"></i>
                    </div>
                </div>
                <div class="form-group">
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false">Ustaw kamerę <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="WorkTable.setCameraPosition(0, 5);">z przodu</a></li>
                            <li><a href="#" onclick="WorkTable.setCameraPosition(0, 0);">z góry</a></li>
                            <li><a href="#" onclick="WorkTable.setCameraPosition(Math.PI / 2, 5);">z prawej</a></li>
                            <li><a href="#" onclick="WorkTable.setCameraPosition(-Math.PI / 2, 5);">z lewej</a></li>
                            <li><a href="#" onclick="WorkTable.setCameraPosition(Math.PI, 5);">z tyłu</a></li>
                        </ul>
                    </div>
                </div>


                <ul>
                    <li><strong>undo / redo</strong></li>
                </ul>
            </div>
        </div>

        <div class="ibox">
            <div class="ibox-title collapse-link">
                <i class="fa fa-chevron-up"></i> Ustawienia sceny
            </div>
            <div class="ibox-content">
                <div class="form-group add-margin-left">
                    <input type="checkbox" name="perspective" id="perspective" onclick="WorkTable.changePerspective(this);">
                    <label for="perspective"><span class="checkbox">Perspektywa</span></label>
                </div>
                <div class="form-group">
                    <div class="range-wrapper">
                        <input name="gridPrecision" id="gridPrecision" type="range" min="1" max="9" step="1" value="3" oninput="WorkTable.setGridPrecision(this.value);" onchange="WorkTable.setGridPrecision(this.value);"/>
                        <i class="fa fa-plus-circle"></i> Siatka <i class="fa fa-minus-circle"></i>
                        <span id="gridPrecisionValue">5</span> cm
                    </div>
                </div>
                <div class="form-group add-margin-left">
                    <input type="checkbox" name="snapToGrid" id="snapToGrid" checked="checked" onclick="WorkTable.changeSnapToGrid(this);">
                    <label for="snapToGrid"><span class="checkbox">Przyciągaj do siatki</span></label>
                </div>
                <ul>
                    <li>wysokość ścian</li>
                    <li>pokazywanie różnych wskaźników, które wyjdą w trakcie: konty, metry, kierunek świata itp.</li>
                    <li>Ustawienia pory doby / zmiana oświetlenia</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="toolbar-wrapper workarea toolbar-objects show">
        <div class="ibox">
            <div class="ibox-title collapse-link">
                <i class="fa fa-chevron-up"></i> Pomieszczenia
            </div>
            <div class="ibox-content">
                <div class="form-group add-margin-left">
                    <input type="checkbox" name="toggleInsertWallMode" id="toggleInsertWallMode" onclick="WorkTable.toggleInsertMode(this, 'walls');">
                    <label for="toggleInsertWallMode"><span class="checkbox">Dodawanie ścian</span></label>
                </div>

                <ul>
                    <li>dodaj ściany punktowo</li>
                    <li>dodaj gotowy pre-set pomieszczenia</li>

                </ul>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{ HTML::script('js/worktable.js') }}
    {{ HTML::script('js/babylon.custom.js') }}
@endsection