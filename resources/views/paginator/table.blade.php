<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                {{--<div class="ibox-title">--}}
                    {{--<h5><i class="fa fa-home"></i> Moje nieruchomości</h5>--}}
                    {{--<div class="ibox-tools">--}}

                        {{--<a class="collapse-link">--}}
                        {{--<i class="fa fa-chevron-up"></i>--}}
                        {{--</a>--}}

                        {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">--}}
                        {{--<i class="fa fa-wrench"></i>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu dropdown-user">--}}
                        {{--<li><a href="#">Config option 1</a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Config option 2</a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}

                        {{--<a class="close-link">--}}
                        {{--<i class="fa fa-times"></i>--}}
                        {{--</a>--}}
                        {{--<a class="btn btn-primary" title="Dodaj mieszkanie" href="{{ url('/properties/add') }}"><i class="fa fa-plus-square"></i> Dodaj</a>--}}
                        {{--@if (isset($theadButtons))--}}
                            {{--@foreach($theadButtons as $button)--}}
                                {{----}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    {{--</div>--}}
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                @foreach ($columns as $column)
                                    <th>{{ $column }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($rows as $row)
                                <tr>
                                    @foreach ($row as $value)
                                        <td>{{ $value }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>

@section('styles')
    {{ HTML::style('css/datatables.min.css') }}
@endsection

@section('scripts')
    {{ HTML::script('js/datatables.min.js') }}

    <script>
        $(document).ready(function () {
            $('table').DataTable();
        });
    </script>
@endsection