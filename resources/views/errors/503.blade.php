<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Not So Flat :: 503</title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/vendor.css') }}
    {{ HTML::style('css/app.css') }}
    {{ HTML::style('css/main.css') }}

</head>

<body class="gray-bg gray-radial-gradient top-navigation">
<div class="middle-box text-center animated fadeInDown">
    <h1>503</h1>
    <h3 class="font-bold">Usługa niedostępna</h3>
</div>
</body>

</html>
