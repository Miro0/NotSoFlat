@extends('layouts.app')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInUp">
        <div>
            <div class="login-header">
                <img src="{!! asset('/img/logo.svg') !!}"/>
                <img src="{!! asset('/img/logo_text.svg') !!}"/>
            </div>

            <div class="ibox">
                <div class="ibox-title background-light-gray">
                    <h2 class="even-margin">Logowanie</h2>
                </div>
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-pulse"></div>
                    <form class="m-t" role="form" action="{!! url('/ajax/login') !!}" id="loginForm" method="POST">
                        <div class="form-group">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Adres email" required="true">
                        </div>
                        <div class="form-group">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Hasło" required="true" minlength="6">
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="remember" id="remember"/>
                            <label for="remember"><span class="checkbox">Zapamiętaj mnie</span></label>
                        </div>
                        {!! csrf_field() !!}
                        <button id="submitButton" type="submit" class="btn btn-primary block full-width m-b">Zaloguj
                        </button>
                        <hr/>
                        <div class="form-group">
                            <table class="table no-top-border">
                                <tr>
                                    <td class="no-top-border">
                                        <p class="text-muted text-center">
                                            <small>Zapomniałeś hasła?</small>
                                        </p>
                                        <a class="btn btn-sm btn-white btn-block" href="{!! url('/forgotpassword') !!}">Resetuj
                                            hasło</a>
                                    </td>
                                    <td class="no-top-border">
                                        <p class="text-muted text-center">
                                            <small>Nie masz jeszcze konta?</small>
                                        </p>
                                        <a class="btn btn-sm btn-white btn-block" href="{!! url('/register') !!}">Zarejestruj</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="panel-footer background-light-gray">
                    &copy; BeedVision 2017
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{ HTML::script('js/jquery.validate.min.js') }}
    {{ HTML::script('js/form.js') }}

    @if($logout)
        <script type="text/javascript">
            jQuery(document).ready(function() {
                swal({
                    title: 'Wylogowano!',
                    type: 'success',
                    confirmButtonColor: '#74858F',
                    confirmButtonText: 'Zamknij',
                    closeOnConfirm: true
                });
            });
        </script>
    @endif
@endsection