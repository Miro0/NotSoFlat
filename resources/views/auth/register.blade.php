@extends('layouts.app')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInUp">
        <div>
            <div class="login-header">
                <img class="main-logo" src="{!! asset('/img/logo.svg') !!}"/><br />
                <img class="text-logo" src="{!! asset('/img/logo_text.svg') !!}"/>
            </div>

            <div class="ibox">
                <div class="ibox-title background-light-gray">
                    <h2 class="even-margin">Rejestracja</h2>
                </div>
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-pulse"></div>
                    <form class="m-t" role="form" action="{!! url('/ajax/register') !!}" id="registerForm" method="POST"">
                        <div class="form-group">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Adres email" required="true">
                        </div>
                        <div class="form-group">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Hasło" required="true" minlength="6">
                        </div>
                        <div class="form-group">
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Potwierdź hasło" required="true" minlength="6">
                        </div>
                        {!! csrf_field() !!}
                        <button id="submitButton" type="submit" class="btn btn-primary block full-width m-b">Zarejestruj</button>
                        <hr/>
                        <p class="text-muted text-center">
                            <small>Posiadasz już konto?</small>
                        </p>
                        <a class="btn btn-sm btn-white btn-block half-centered" href="{!! url('/login') !!}">Zaloguj</a>
                    </form>
                </div>
                <div class="panel-footer background-light-gray">
                    &copy; BeedVision 2017
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{ HTML::script('js/jquery.validate.min.js') }}
    {{ HTML::script('js/form.js') }}
@endsection