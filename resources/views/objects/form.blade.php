@extends('layouts.app')

@section('content')

<style>
    .input-group {
        width: 100%;
    }

    .input-group-addon {
        width: 50%;
    }

    .form-control {
        width: 100%;
    }
</style>

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if(isset($object) && isset($objectId))
{!! FormView::startForm('object/save/' . $objectId) !!}
{!! FormView::addData($object) !!}
@else 
{!! FormView::startForm('object/save') !!}
@endif

<table class="table table-hover">
    <thead>
        <tr>
            <th class="success" colspan="2">
                @if(isset($object))
                Edytuj obiekt
                @else
                Dodaj nowy obiekt
                @endif
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th colspan="2">Dane ogólne</th>
        </tr>
        <tr>
            <td colspan="2">
                {!! FormView::input('Typ elementu', 'type', 'text') !!}
            </td>
        </tr>
        <tr>
            <th>Dane projektowania</th>
            <th>Dane renderowania</th>
        </tr>
        <tr>
            <td style='width: 50%;'>
                {!! FormView::input('Nazwa elementu', 'design_data[name]', 'text') !!}
                {!! FormView::select('Ilość punktów kształtu', 'design_data[shapePointsAmount]', [2=>2, 4=>4]) !!}
                {!! FormView::input('Rozmiar początkowy', 'design_data[initSize]', 'number') !!}
                {!! FormView::input('Baza warstwy (wysokość)', 'design_data[layerBase]', 'number') !!}
                {!! FormView::input('Minimalna wysokość', 'design_data[minHeight]', 'number') !!}
                {!! FormView::input('Maksymalna wysokość', 'design_data[maxHeight]', 'number') !!}
                {!! FormView::input('Posiada kierunek', 'design_data[isDirectional]', 'checkbox') !!}
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan='2'>
                <button type="submit" class="btn btn-success">Zapisz</button>
            </td>
        </tr>
    </tbody>
</table>

{!! FormView::endForm(); !!}

@endsection