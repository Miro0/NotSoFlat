@extends('layouts.app')

@section('content')

@if (session('success') == 'add')
<div class='alert alert-success'>Dodano nowy objekt</div>
@elseif (session('success') == 'edit')
<div class='alert alert-success'>Edytowano objekt</div>
@endif

<table class="table table-success table-hover table-striped">
    <thead>
        <tr>
            <th colspan='100' class='success'>Objekty</th>
        </tr>
        <tr>
            <th>Typ</th>
            <th>Dane projektowania</th>
            <th>Dane renderowania</th>
            <th>Opcje</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($objects as $object)
        <?php
        $designData = unserialize($object->design_data);
        ?>
        <tr>
            <td>{{$object->type}}</td>
            <td>
                <small>
                    <ul>
                        @foreach ($designData as $name => $value)
                        <li>{{$name}} = {{$value}}</li>
                        @endforeach
                    </ul>
                </small>
            </td>
            <td>{{$object->renderData}}</td>
            <td><a class='btn btn-primary' href='{{url('object/form/' . $object->id)}}'>Edytuj</a></td>
        </tr>
        @endforeach
    </tbody>
</table>


<br />
<a class='btn btn-primary' href='{{url('object/form')}}'>Dodaj objekt</a>
@endsection