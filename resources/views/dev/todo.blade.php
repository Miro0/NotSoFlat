@extends('layouts.app')

@section('content')

<style>
    .green {
        color: #66cdaa;
    }

    .orange {
        color: #ff7256;
    }
</style>

<?php foreach ($listItems as $panelHeading => $panelData) : ?>
    <div class="panel panel-info">
        <div class="panel-heading"><?= $panelHeading ?></div>
        <div class="panel-body">
            <?php foreach ($panelData as $listRow): ?>
                <?= $listRow ?>
                <br />
            <?php endforeach; ?>  
        </div>
    </div>
<?php endforeach; ?>

@endsection
