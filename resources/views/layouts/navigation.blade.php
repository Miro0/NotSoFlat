<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">Example user</strong>
                            </span> <span class="text-muted text-xs block">Opcje <b class="caret"></b></span>
                        </span>
                    </a>
                    @include('layouts.user_dropdown')
                </div>
                <div class="logo-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-user-circle-o"></i>
                    </a>
                    @include('layouts.user_dropdown')
                </div>
            </li>

            <li class="{{ isActiveRoute('/') }}" title="Strona główna">
                <a href="{{ url('/') }}"><i class="fa fa-circle"></i> <span class="nav-label">Strona główna</span></a>
            </li>
            <li class="{{ isActiveRoute('properties/show') }}" title="Moje nieruchomości">
                <a href="{{ url('/properties/show') }}"><i class="fa fa-home"></i> <span
                            class="nav-label">Moje nieruchomości</span> </a>
            </li>
            <li class="{{ isActiveRoute('object/index') }}" title="Moje ogłoszenia">
                <a href="{{ url('/object/index') }}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Moje ogłoszenia</span>
                </a>
            </li>
        </ul>

    </div>
</nav>
