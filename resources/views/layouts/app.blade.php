<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Not So Flat</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/vendor.css') }}
    {{ HTML::style('css/app.css') }}
    {{ HTML::style('css/jquery-ui.min.css') }}
    {{ HTML::style('css/jquery-ui.theme.min.css') }}
    {{ HTML::style('css/sweetalert.css') }}
    {{ HTML::style('css/studio.css') }}
    {{ HTML::style('css/main.css') }}

    @yield('styles')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="notsoflat 3d flat visualization home virtual walk">
    <meta name="author" content="BeedVision">
    <link rel="icon" type="image/png" href="{{ asset('/favicon.png') }}">
</head>

@if(Auth::user())
    <body class="gray-bg top-navigation">
    @else
        <body class="gray-bg gray-radial-gradient top-navigation">
        @endif


        {{--{{ var_dump(session('url.intended')) }}--}}
        @if (!Auth::user())
            <div id="wrapper force-hide-scroll">
                @else
                    <div id="wrapper" class="one-screen">
                        @endif

                        @if (Auth::user())
                            {{--@include('layouts.navigation')--}}
                            <div id="page-wrapper" class="gray-bg one-screen">
                                @include('layouts.topnavbar')
                                @endif

                                @yield('content')

                                @if(Auth::user())
                                    @include('layouts.footer')
                            </div>
                    </div>
        @endif

        {{ HTML::script('js/jquery.min.js') }}
        {{ HTML::script('js/jquery-ui.min.js') }}
        {{--{{ HTML::script('js/bootstrap.min.js') }}--}}
        {{ HTML::script('js/app.js') }}
        {{ HTML::script('js/sweetalert.min.js') }}
        {{ HTML::script('js/Common/JSUtils.js') }}

        @yield('scripts')

        </body>
</html>