<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <i class="fa fa-reorder"></i>
            </button>
            <img class="logo " src="{!! asset('/img/logo.svg') !!}" />
            <img class="logo text " src="{!! asset('/img/logo_text.svg') !!}" />
        </div>

        <div class="navbar-collapse collapse" id="navbar" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/properties/add') }}">
                        <i class="fa fa-plus"></i> Dodaj nieruchomość
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="{{ url('/logout') }}">
                        Wyloguj <i class="fa fa-sign-out"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
