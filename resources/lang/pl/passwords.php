<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasło musi posiadać minimum 6 znaków.',
    'reset' => 'Zresetowano hasło.',
    'sent' => 'Wiadomość z linkiem resetującym hasło został wysłany na Twoją skrzynkę pocztową.',
    'token' => 'Nieprawidłowy token resetujący hasło.',
    'user' => "Nie znaleziono użytkownika z podanym adresem email.",

];
