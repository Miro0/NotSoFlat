<?php

namespace App\Helpers\View;

class FormViewHelper
{

    protected static $_data = array();

    public static function startForm($url, $method = 'POST', $id = '', $class = '') {
        $return = '<form role="form" action="' . url($url) . '" method="' . $method . '" id="' . $id . '" class="' . $class . '">';
        $return .= csrf_field();

        return $return;
    }

    public static function endForm() {
        if (isset(self::$_data[csrf_token()])) {
            unset(self::$_data[csrf_token()]);
        }
        $return = '</form>';

        return $return;
    }

    public static function addData($data) {
        if (!is_array($data)) {
            $data = $data->toArray();
        }

        self::$_data[csrf_token()] = self::_flatArray($data);
    }

    public static function input($label, $name, $type = 'text', $setValue = null) {
        self::_checkValue($setValue, $name);
        $return = '<div class="input-group">';
        $return .= '<span class="input-group-addon">' . $label . '</span>';
        $return .= '<input class="form-control" type="' . $type . '" name="' . $name . '" ';
        if (!empty($setValue) || $setValue === 0) {
            if ($type !== 'checkbox') {
                $return .= 'value="' . $setValue . '" ';
            } else if ($setValue === 'on') {
                $return .= 'checked="checked"';
            }
        }
        $return .= ' />';
        $return .= '</div>';

        return $return;
    }

    public static function select($label, $name, $options, $setValue = null) {
        self::_checkValue($setValue, $name);
        $return = '<div class="input-group">';
        $return .= '<span class="input-group-addon">' . $label . '</span>';
        $return .= '<select class="form-control" name="' . $name . '">';
        foreach ($options as $value => $text) {
            $return .= '<option value="' . $value . '"';
            if (!empty($setValue) && $setValue == $value) {
                $return .= ' selected="selected"';
            }
            $return .= '>' . $text . '</option>';
        }
        $return .= '</select>';
        $return .= '</div>';

        return $return;
    }

    protected static function _flatArray($array, $parentKey = null) {
        $return = array();
        foreach ($array as $key => $value) {
            if ($parentKey) {
                $key = $parentKey . '[' . $key . ']';
            }
            if (!is_array($value)) {
                $return[$key] = $value;
            } else {
                $return = array_merge($return, self::_flatArray($value, $key));
            }
        }

        return $return;
    }

    protected static function _checkValue(&$value, $name) {
        if (!$value) {
            if (isset(self::$_data[csrf_token()][$name])) {
                $value = self::$_data[csrf_token()][$name];
            }
        }
    }

}
