<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @array
     */
    protected $fillable = array(
        'type',
        'street',
        'city'
    );

}
