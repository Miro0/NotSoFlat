<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    if (empty(Auth::user())) {
        return redirect('/login');
    }
    return view('layouts/app');
});

Route::get('register', 'Auth\AuthController@registerForm');
Route::get('login', 'Auth\AuthController@loginForm');
Route::get('logout', 'UserController@logout');

Route::group(['prefix' => 'ajax/'], function () {
    Route::post('register', 'Auth\AuthController@ajaxRegister');
    Route::post('login', 'Auth\AuthController@ajaxLogin');
});

Route::get('properties/show', 'PropertiesController@showList')->middleware('auth');
Route::get('properties/add', 'PropertiesController@add')->middleware('auth');

Route::get('todo', 'DevController@todo')->middleware('auth');

Route::get('studio/design', 'StudioController@design')->middleware('auth');
Route::post('studio/render', 'StudioController@render')->middleware('auth');

Route::get('object/index', 'ObjectController@index')->middleware('auth');
Route::get('object/form', 'ObjectController@form')->middleware('auth');
Route::get('object/form/{object_id}', 'ObjectController@form')->middleware('auth');
Route::post('object/save', 'ObjectController@save')->middleware('auth');
Route::post('object/save/{id}', 'ObjectController@save')->middleware('auth');
//Route::delete('object/{object}', 'ObjectController@delete');
