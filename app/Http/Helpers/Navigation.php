<?php


function isActiveRoute($route, $output = 'active')
{
    if (\Route::getCurrentRoute()->getUri() == $route) {
        return $output;
    }
}
