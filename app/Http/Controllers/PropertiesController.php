<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Property;

class PropertiesController extends Controller
{

    public function showList()
    {
        $properties = Property::paginate(15);
        return view('properties/show_list', ['properties' => $properties]);
    }

    public function add(Request $request)
    {

        return view('properties/add');
    }

}
