<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Objects;

class StudioController extends Controller
{

    public function design() {

        $objects = Objects::unserializeData(Objects::all()->toArray());
        $viewData = array(
            'json' => json_encode($objects),
            'data' => array()
        );
        foreach($objects as $object) {
            $viewData['data'][] = array(
                'type' => $object['type'],
                'value' => $object['design_data']['name']
            );
        }
        
        return view('studio.design', $viewData);
    }

    public function render(Request $request) {
        
        return view('studio.render', ['renderData' => $request->data]);
    }

}
