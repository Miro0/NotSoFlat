<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Objects;

class ObjectController extends Controller
{

    public function index() {
        return view('objects.index', ['objects' => Objects::all()]);
    }

    public function form(Request $request, $id = null) {
        if (!empty($id)) {
            if (!empty($request->session()->getOldInput())) {
                $object = $request->session()->getOldInput();
            } else {
                $object = Objects::whereId($id)->first();
                $object->design_data = unserialize($object->design_data);
            }
            return view('objects.form', ['object' => $object, 'objectId' => $id]);
        } else {
            return view('objects.form');
        }
    }

    public function save(Request $request, $id = null) {
        $rules = array(
            'type' => 'required|max:30',
            'design_data.name' => 'required|max:30',
            'design_data.shapePointsAmount' => 'required|regex:/[2,4]/i|integer',
            'design_data.initSize' => 'required|integer',
            'design_data.layerBase' => 'required|integer',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return redirect('object/form/' . $id)
                            ->withErrors($messages)
                            ->withInput($request->all());
        } else {
            if ($id) {
                $saveType = 'edit';
                Objects::where('id', $id)
                        ->update([
                            'type' => $request->type,
                            'design_data' => self::_serializeData($request->design_data)
                ]);
            } else {
                $saveType = 'add';
                $newObject = new Object;
                $newObject->type = $request->type;
                $newObject->design_data = self::_serializeData($request->design_data);
                /**
                 * @todo Zdefiniować jakie mają zapisywać się dane dotyczące renderowania
                 */
                $newObject->render_data = 'todo';
                $newObject->save();
            }

            return redirect('object/index')
                            ->with('success', $saveType);
        }
    }

    private function _serializeData($data) {
        $notIntKey = array('name');
        foreach ($data as $key => $value) {
            if (!in_array($key, $notIntKey)) {
                $data[$key] = is_numeric($value) ? intval($value) : $value;
            }
        }
        return serialize($data);
    }

}
