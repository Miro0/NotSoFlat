<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class DevController extends Controller
{

    public function todo() {
        $listItems = array(
            'Studio::Design' => array(
                '[ZROBIONE] Dodać łatwiejszy dostęp do zmiennych obiektu / refaktoryzacja Studio::Design.js',
                '[ZROBIONE] Dodać informację o ilościu punktów składowych elementu',
                '[ZROBIONE] Wykrywanie kolizji',
                '[ZROBIONE] Możliwości zmiany rozmiaru i położenia elementu mają tylko aktywne elementy',
                '[ZROBIONE] Optymalizacja akcji DRAG, aby wykonywała się tylko 1 na ruch po kratce',
                'Elementy powinny mieć dodatkowy typ wykrywalny w kolizjach: naścienny, naścienny kierunkowy? (obraz, plakat itp.), przyścienny, wewnątrzpokojowy',
                '[ZROBIONE] Zmienić object.name na TYPE, a object.plName na VALUE lub coś w den deseń / refaktor',
                '[ZROBIONE] Usuwanie objektów',
                'Obliczanie pola pomieszczeń w oparciu o kolizje',
                '[ZROBIONE] Tworzenie powiązań CONSTRAINT między elementami w oparciu o kolizję (wiązania są zawsze względem pokoi)',
                '[ZROBIONE] Usuwanie CONSTRAINT ma ZAWSZE id ostatniego elementu z pętli dodawania przycisków',
                '[ZROBIONE] Usuwanie przykładowo 2 z 3 powiązań usuwa 2 wiązania - poprawić',
                'Realizowanie powiązań CONSTRAINT łańcuchowo (wiązania wiązań mają być również brane pod uwagę)',
                '[W TRAKCIE...] Ujednolicony sposób rozszerzania elementów o kolejne cechy',
                'Dodanie dodatkowych cech elementów w panelu narzędzi',
                'Dodanie elementom cech otwierania (drzwi po łuku, przesuwne, okna po łuku lub do góry - uchylne)',
                'Dodanie menu kontekstowego w polu roboczym w oparciu o możliwości w Oknie Akcji',
                'Dodanie modyfikowania pola roboczego - rozmiar, skala',
                'Uwzględnić zmianę rozdzielczości ekranu',
                '[ZROBIONE] Pokoje - który ma być ponad którym - określić przydatność i cel warstw',
                'Wszystkie elementy mogące posiadać różną wysokość - dodać to (w Oknie Akcji i zdefiniować style na wysokość)',
                'Normalizacja styli - wspólne cechy elementów poskładać do wspólnych klas - klasy elementów maja zawierać tylko wyróżniające się style',
                'Ostylować elementy ui-resizable, aby łatwiej można było korzystać z uchwytów i aby były lepiej widoczne',
                'Do wymiarów dodać możliwość edycji i po zatwierdzeniu zmian zmieniać rozmiar elementu wobec wskazanych danych',
                'Dodać inny tryb dodwania przedmiotów - po kliknięciu wskazujemy dopiero, gdzie element ma zostać dodany',
                'Dodawanie pojedynczych ścian - pomysł + logika obliczeniowa kolizji, pól powierzchni',
                'Undo / Redo',
                'Zapisywanie stanu projektu w razie braku internetu czy jakiejkolwiek innej awarii - możliwość "przywrócenia ostatniej zapisanej sesji" - powiązane z zadaniem Undo / Redo (LocalStorage)',
                'Coś jest jeszcze nie tak z usuwaniem przedmiotów (przypadek zostają 2 pokoje i usuwam pokój nr 1)'
            ),
            'Studio::Panel' => array(
                'Optymalizacja aktualizacji paneli - nie robić akcji, jeśli panel do wygenerowania jest taki sam',
            ),
            'Studio::Render' => array(
                'Przechwycić dane',
                'Zdefiniować wygląd danych do renderu (stworzyć skrypt Studio::ObjectManager do definiowania obiektów)',
                '[W TRAKCIE...] Znaleźć sposób na ujednolicony system definiowania elementów w renderze, jak i projektorze (może dodatkowe narzędzie ?)'
            ),
            
            
            'OTHER' => array(
                'Panel administratora',
                'Moduł z ogłoszeniami',
                'Strona informacyjna'
            )
        );

        foreach ($listItems as $mainKey => $listItemsInCategory) {
            foreach ($listItemsInCategory as $categoryKey => $listRow) {
                $regex = '/^\[(.*)\](.*)$/i';
                $matches = false;
                preg_match($regex, $listRow, $matches);
                if (empty($matches)) {
                    $className = '';
                    $value = $listRow;
                    $icon = 'minus';
                } else {
                    switch ($matches[1]) {
                        case 'ZROBIONE':
                            $className = 'green';
                            $icon = 'ok';
                            break;
                        case 'W TRAKCIE...':
                            $className = 'orange';
                            $icon = 'flash';
                            break;
                    }
                    $value = $matches[2];
                }
                $listItems[$mainKey][$categoryKey] = "<span class='$className'><i class='glyphicon glyphicon-$icon'></i> $value</span>";
            }
        }
        
        return view('dev.todo', ['listItems' => $listItems]);
        
    }

}
