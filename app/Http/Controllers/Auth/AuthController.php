<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return mixed
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    public function registerForm()
    {
        return view('auth.register');
    }

    public function ajaxRegister(Request $request)
    {
        $userExist = User::where(['email' => $request->email])->first();
        if (!empty($userExist)) {
            return response()->json([
                'title' => 'Konto istnieje',
                'text' => 'Podany adres email został już zarejestrowany w systemie',
                'type' => 'error',
                'addRules' => [[
                    'selector' => '#email',
                    'rule' => 'notEqualTo',
                    'value' => $request->email
                ]]
            ]);
        } else {
            if ($this->validator($request->all())) {
                if ($this->create($request->all()) && Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    return response()->json([
                        'title' => 'Utworzono konto!',
                        'type' => 'success',
                        'redirect' => url('/'),
                        'confirmButtonText' => 'Kontynuuj'
                    ]);
                } else {
                    return response()->json([
                        'title' => 'Wystąpił błąd z utworzeniem konta',
                        'type' => 'error'
                    ]);
                }
            } else {
                return response()->json([
                    'title' => 'Błędne dane',
                    'type' => 'error'
                ]);
            }
        }
    }

    public function loginForm()
    {
        $wasLogout = isset($_GET['logout']) && $_GET['logout'] == 1;
        return view('auth.login', ['logout' => $wasLogout]);
    }

    public function ajaxLogin(Request $request)
    {
        if ($request->remember === 'on') {
            $remember = true;
        }  else {
            $remember = false;
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
            if (!empty(session('url.intended'))) {
                $redirectUrl = session()->pull('url.intended');
            } else {
                $redirectUrl = url('/');
            }
            return response()->json([
                'title' => 'Zalogowano!',
                'type' => 'success',
                'redirect' => $redirectUrl,
                'confirmButtonText' => 'Kontynuuj'
            ]);
        } else {
            return response()->json([
                'title' => 'Nieprawidłowe dane',
                'type' => 'error',
            ]);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'type' => 'free'
        ]);
    }
}
