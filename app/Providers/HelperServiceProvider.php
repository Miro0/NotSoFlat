<?php

namespace App\Providers;
namespace App\Helpers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('FormViewHelper', function() {
           return new View\FormViewHelper;
        });
        $this->app->bind('Navigation', function() {
            return new Navigation;
        });
    }
}
