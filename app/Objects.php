<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objects extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @array
     */
    protected $fillable = array(
        'type',
        'design_data',
        'render_data'
    );
    
    public static function unserializeData(Array $objects) {
        foreach($objects as $key => $value) {
            $objects[$key]['design_data'] = unserialize($objects[$key]['design_data']);
        }
        
        return $objects;
    }

}
